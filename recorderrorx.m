function [error,maxerror,lengthvecs] = recorderrorx(tp,yp,t,y,handles)
% after final solution is calculated, calculates the error at every point
% and plots this error against time.
% Author: Jacob Novitch
% 3-30-19
lengthvecs = size(y);
error = zeros(lengthvecs(1,1));
maxerror = 0;
for it = 1:lengthvecs(1,1)
    error(it) = abs((yp(it,1)-y(it,1))/y(it,1));
    if error(it)>maxerror
        maxerror = error(it);
    end
end
end
