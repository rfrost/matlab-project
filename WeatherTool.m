function varargout = WeatherTool(varargin)
% WEATHERTOOL MATLAB code for WeatherTool.fig
%      WEATHERTOOL, by itself, creates a new WEATHERTOOL or raises the existing
%      singleton*.
%
%      H = WEATHERTOOL returns the handle to a new WEATHERTOOL or the handle to
%      the existing singleton*.
%
%      WEATHERTOOL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WEATHERTOOL.M with the given input arguments.
%
%      WEATHERTOOL('Property','Value',...) creates a new WEATHERTOOL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before WeatherTool_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to WeatherTool_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help WeatherTool

% Last Modified by GUIDE v2.5 22-Apr-2019 13:18:48

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @WeatherTool_OpeningFcn, ...
                   'gui_OutputFcn',  @WeatherTool_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before WeatherTool is made visible.
function WeatherTool_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to WeatherTool (see VARARGIN)

% Choose default command line output for WeatherTool
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes WeatherTool wait for user response (see UIRESUME)
% uiwait(handles.figure1);

if handles.SelectxVar.Value == 1
    conditionx(handles);
elseif handles.SelectyVar.Value == 1
    conditiony(handles);
elseif handles.SelectzVar.Value == 1
    conditionz(handles)
end
% --- Outputs from this function are returned to the command line.
function varargout = WeatherTool_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;





function timeInput_Callback(hObject, eventdata, handles)
% hObject    handle to timeInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of timeInput as text
%        str2double(get(hObject,'String')) returns contents of timeInput as a double

% --- Executes during object creation, after setting all properties.
function timeInput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to timeInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function errorInput_Callback(hObject, eventdata, handles)
% hObject    handle to errorInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of errorInput as text
%        str2double(get(hObject,'String')) returns contents of errorInput as a double

% --- Executes during object creation, after setting all properties.
function errorInput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to errorInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in doAnimate.
function doAnimate_Callback(hObject, eventdata, handles)
% hObject    handle to doAnimate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of doAnimate


% --- Executes when selected object is changed in chooseVar.
function chooseVar_SelectionChangedFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in chooseVar 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% --- Executes on butto n press in calcButton.

function calcButton_Callback(hObject, eventdata, handles)
% hObject    handle to calcButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.SelectxVar.Value == 1
    conditionx(handles);
elseif handles.SelectyVar.Value == 1
    conditiony(handles);
elseif handles.SelectzVar.Value == 1
    conditionz(handles);
end


% --- Executes on button press in doSmooth.
function doSmooth_Callback(hObject, eventdata, handles)
% hObject    handle to doSmooth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of doSmooth
