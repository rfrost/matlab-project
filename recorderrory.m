function [error, maxerror,lengthvecs] = recorderrory(tp,yp,t,y)
% after final solution is calculated, calculates the error at every point
% and plots this error against time.
% Author: Jacob Novitch
% 3-30-19
lengthvecs = size(y);
error = zeros(lengthvecs(1,1));
maxerror = 0;
for it = 1:lengthvecs(1,1)
    error(it) = abs((yp(it,2)-y(it,2))/y(it,2));
    if error(it)>maxerror
        maxerror = error(it);
    end
end
end