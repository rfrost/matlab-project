function conditionz(handles)
% use this function to create the graphs and determine the error when the z
% initial condition is varied and x and y remain constant
% Authors: Ryan Frost Grayson Zinn, Jacob Novitch
% 3-26-19
%% Set Parameters
x0 = 5; 
y0 = 5;
z0 = 5;
f0 = [x0,y0,z0]; % set of initial conditions
beta = 8/3;
sigma = 10;
rho = 28;
odesys = @(t,y) [sigma*(-y(1)+y(2)); rho*y(1)-y(2)-y(1)*y(3);...
 -1*beta*y(3)+y(1)*y(2)];


%% User Input
time = str2double(handles.timeInput.String);
error = str2double(handles.errorInput.String);

%% System Solver
[t,y] = ode45(odesys,[0,time],f0);


sizeconstantsolution = size(y);
z1 = 5.01;
perturbance = z1-z0;
actualerrorz1 = 1;
actualerrorz2 = 1;
multiplier = 1;
maxerror = 0;
odesys2 = @(tp,yp) [sigma*(-yp(1)+yp(2)); rho*yp(1)-yp(2)-yp(1)*yp(3);...
 -1*beta*yp(3)+yp(1)*yp(2)];
doanimate = handles.doAnimate.Value;

while abs(actualerrorz1)>error || abs(actualerrorz2)>error && z1>z0
f1 = [x0,y0,z1]; %  perturbed initial conditions
[tp,yp] = ode45(odesys2,[0,time],f1);
sizesolution = size(yp);
actualerrorz1 = (yp(sizesolution(1,1))-y(sizeconstantsolution(1,1)))/y(sizeconstantsolution(1,1));
actualerrorz2 = (yp(round(sizesolution(1,1)/2))-y(round(sizeconstantsolution(1,1)/2)))/y(round(sizeconstantsolution(1,1)/2));
z1 = z1 - 0.000001*(multiplier);
z1Array(1, multiplier) = abs(actualerrorz1);
perturbArray(1, multiplier) = z1-z0;
if abs(actualerrorz1)>maxerror
    maxerror = abs(actualerrorz1);
end
multiplier = multiplier+1;
end
%% Generating Reccomendation
accuracy = abs((z1-z0)/z0);
handles.reccomendText.String = ['The z initial condition must be within ' num2str(accuracy) ...
    '% of the exact z initial condition to generate a solution with the specified accuracy.'];
%% Plotting


if doanimate
for it = 1:sizesolution(1,1)
plot(handles.solutionPlot,t,y(:,3),'r.-',tp(it),yp(it,3),'bo',tp(1:it),yp(1:it,3),'b-');
xlabel(handles.solutionPlot,'t(days)');
ylabel(handles.solutionPlot,'z');
legend(handles.solutionPlot, 'Exact Solution','Calculated Solution');
title(handles.solutionPlot,'Solution Graph');
drawnow
end

[ploterror, maxerror,lengthvecs] = recorderrorz(tp,yp,t,y);
for it = 1:lengthvecs(1,1)
plot(handles.errorPlot,t(it),ploterror(it),'bo',t(1:it),ploterror(1:it),'r-');
axis(handles.errorPlot,[0,tp(lengthvecs(1,1)),0,maxerror]);
xlabel(handles.errorPlot,'t(days)');
ylabel(handles.errorPlot,'Error');
title(handles.errorPlot,'Error Graph');
drawnow
end

plot(handles.perturbPlot,perturbArray,z1Array)
    xlabel(handles.perturbPlot,'Perturbance');
    ylabel(handles.perturbPlot,'Error');
    title(handles.perturbPlot,'Perturbance vs. Error');
elseif ~doanimate
    plot(handles.solutionPlot,t,y(:,3),'r.-',tp,yp(:,3))
    xlabel(handles.solutionPlot,'t(days)');
    ylabel(handles.solutionPlot,'z');
    legend(handles.solutionPlot, 'Exact Solution','Calculated Solution');
    title(handles.solutionPlot,'Solution Graph');
    
    ploterror = recorderrorz(tp,yp,t,y);
    plot(handles.errorPlot,t,ploterror);
    xlabel(handles.errorPlot,'t(days)');
    ylabel(handles.errorPlot,'Error');
    title(handles.errorPlot,'Error Graph');
    
    plot(handles.perturbPlot,perturbArray,z1Array)
    xlabel(handles.perturbPlot,'Perturbance');
    ylabel(handles.perturbPlot,'Error');
    title(handles.perturbPlot,'Perturbance vs. Error');
end
end