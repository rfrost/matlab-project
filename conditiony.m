function conditiony(handles)
% use this function to create the graphs and determine the error when the y
% initial condition is varied and x and z remain constant
% Author: Ryan Frost Grayson Zinn, Jacob Novitch
% 3-26-19

%% Set Parameters
x0 = 5; 
y0 = 5;
z0 = 5;
f0 = [x0,y0,z0]; % set of initial conditions
beta = 8/3;
sigma = 10;
rho = 28;
odesys = @(t,y) [sigma*(-y(1)+y(2)); rho*y(1)-y(2)-y(1)*y(3);...
 -1*beta*y(3)+y(1)*y(2)];

%% User Input
time = str2double(handles.timeInput.String);
error = str2double(handles.errorInput.String);

%% System Solver
[t,y] = ode45(odesys,[0,time],f0);


sizeconstantsolution = size(y);
y1 = 5.2;
perturbance = y1-y0;
actualerrory1 = 1;
actualerrory2 = 1;
multiplier = 1;
maxerror = 0;
odesys2 = @(tp,yp) [sigma*(-yp(1)+yp(2)); rho*yp(1)-yp(2)-yp(1)*yp(3);...
 -1*beta*yp(3)+yp(1)*yp(2)];
doanimate = handles.doAnimate.Value;

while abs(actualerrory1)>error || abs(actualerrory2)>error && y1>y0
f1 = [x0,y1,z0]; %  perturbed initial conditions
[tp,yp] = ode45(odesys2,[0,time],f1);
sizesolution = size(yp);
actualerrory1 = (yp(sizesolution(1,1))-y(sizeconstantsolution(1,1)))/y(sizeconstantsolution(1,1));
actualerrory2 = (yp(round(sizesolution(1,1)/2))-y(round(sizeconstantsolution(1,1)/2)))/y(round(sizeconstantsolution(1,1)/2));
y1 = y1 - 0.000001*(multiplier);
y1Array(1, multiplier) = abs(actualerrory1);
perturbArray(1, multiplier) = y1-y0;
if abs(actualerrory1)>maxerror
    maxerror = abs(actualerrory1);
end
multiplier = multiplier+1;
end
%% Generating Reccomendation
accuracy = abs((y1-y0)/y0);
handles.reccomendText.String = ['The y initial condition must be within ' num2str(accuracy) ...
    '% of the exact y initial condition to generate a solution with the specified accuracy.'];
%% Plotting


if doanimate
for it = 1:sizesolution(1,1)
plot(handles.solutionPlot,t,y(:,2),'r.-',tp(it),yp(it,2),'bo',tp(1:it),yp(1:it,2),'b-');
xlabel(handles.solutionPlot,'t');
ylabel(handles.solutionPlot,'y');
legend(handles.solutionPlot, 'Exact Solution','Calculated Solution');
title(handles.solutionPlot,'Solution Graph');
drawnow
end

[ploterror,maxerror,lengthvecs] = recorderrory(tp,yp,t,y);
for it = 1:lengthvecs(1,1)
plot(handles.errorPlot,t(it),ploterror(it),'bo',t(1:it),ploterror(1:it),'r-');
axis(handles.errorPlot,[0,t(lengthvecs(1,1)),0,maxerror]);
xlabel(handles.errorPlot,'t(days)');
ylabel(handles.errorPlot,'Error');
title(handles.errorPlot,'Error Graph');
drawnow
end

plot(handles.perturbPlot,perturbArray,y1Array)
    xlabel(handles.perturbPlot,'Perturbance');
    ylabel(handles.perturbPlot,'Error');
    title(handles.perturbPlot,'Perturbance vs. Error');
elseif ~doanimate
    plot(handles.solutionPlot,t,y(:,2),'r.-',tp,yp(:,2))
    xlabel(handles.solutionPlot,'t(days)');
    ylabel(handles.solutionPlot,'y');
    legend(handles.solutionPlot, 'Exact Solution','Calculated Solution');
    title(handles.solutionPlot,'Solution Graph');
    ploterror = recorderrory(tp,yp,t,y);
    plot(handles.errorPlot,t,ploterror);
    xlabel(handles.errorPlot,'t(days)');
    ylabel(handles.errorPlot,'Error');
    title(handles.errorPlot,'Error Graph');
    plot(handles.perturbPlot,perturbArray,y1Array)
    xlabel(handles.perturbPlot,'Perturbance');
    ylabel(handles.perturbPlot,'Error');
    title(handles.perturbPlot,'Perturbance vs. Error');
end
end