function conditionx(handles)
% use this function to create the graphs and determine the error when the x
% initial condition is varied and y and z remain constant
% Author: Ryan Frost Grayson Zinn, Jacob Novitch
% 3-26-19
%% Set Parameters
x0 = 5; 
y0 = 5;
z0 = 5;
f0 = [x0,y0,z0]; % set of initial conditions
beta = 8/3;
sigma = 10;
rho = 28;
odesys = @(t,y) [sigma*(-y(1)+y(2)); rho*y(1)-y(2)-y(1)*y(3);...
 -1*beta*y(3)+y(1)*y(2)];
doanimate = handles.doAnimate.Value;

%% User Input
time = str2double(handles.timeInput.String);
error = str2double(handles.errorInput.String);

%% System Solver
[t,y] = ode45(odesys,[0,time],f0);
sizeconstantsolution = size(y);
x1 = 5.2;
perturbance = x1-x0;
actualerrorx1 = 1;
actualerrorx2 = 1;
multiplier = 1;
maxerror = 0;
odesys2 = @(tp,yp) [sigma*(-yp(1)+yp(2)); rho*yp(1)-yp(2)-yp(1)*yp(3);...
 -1*beta*yp(3)+yp(1)*yp(2)];

while abs(actualerrorx1)>error || abs(actualerrorx2)>error && x1>x0
f1 = [x1,y0,z0]; %  perturbed initial conditions
[tp,yp] = ode45(odesys2,[0,time],f1);
sizesolution = size(yp);
actualerrorx1 = (yp(sizesolution(1,1))-y(sizeconstantsolution(1,1)))/y(sizeconstantsolution(1,1));
actualerrorx2 = (yp(round(sizesolution(1,1)/2))-y(round(sizeconstantsolution(1,1)/2)))/y(round(sizeconstantsolution(1,1)/2));
x1 = x1 - 0.000001*(multiplier);
x1Array(1, multiplier) = abs(actualerrorx1);
perturbArray(1, multiplier) = x1-x0;
if abs(actualerrorx1)>maxerror
    maxerror = abs(actualerrorx1);
end
multiplier = multiplier+1;
end
%% Generating Reccomendation
accuracy = abs((x1-x0)/x0);
handles.reccomendText.String = ['The x initial condition must be within ' num2str(100*accuracy) ...
    '% of the exact x initial condition to generate a solution with the specified accuracy.'];
%% Plotting
if doanimate
for it = 1:sizesolution(1,1)
plot(handles.solutionPlot,t,y(:,1),'r.-',tp(it),yp(it,1),'bo',tp(1:it),yp(1:it,1),'b-');
xlabel(handles.solutionPlot,'t(days)');
ylabel(handles.solutionPlot,'x');
legend(handles.solutionPlot,'Exact Solution','Calculated Solution');
title(handles.solutionPlot,'Solution Graph');
drawnow
end

[ploterror,maxerror,lengthvecs] = recorderrorx(tp,yp,t,y);
for it = 1:lengthvecs(1,1)
plot(handles.errorPlot,t(it),ploterror(it),'bo',t(1:it),ploterror(1:it),'r-');
axis(handles.errorPlot,[0,t(lengthvecs(1,1)),0,maxerror]);
xlabel(handles.errorPlot,'t(days)');
ylabel(handles.errorPlot,'Error');
title(handles.errorPlot,'Error Graph');
drawnow
end

plot(handles.perturbPlot,perturbArray,x1Array)
    xlabel(handles.perturbPlot,'Perturbance');
    ylabel(handles.perturbPlot,'Error');
    title(handles.perturbPlot,'Perturbance vs. Error');

elseif ~doanimate
    plot(handles.solutionPlot,t,y(:,1),'r.-',tp,yp(:,1))
    xlabel(handles.solutionPlot,'t(days)');
    ylabel(handles.solutionPlot,'x');
    legend(handles.solutionPlot,'Exact Solution','Calculated Solution');
    title(handles.solutionPlot,'Solution Graph');
    ploterror = recorderrorx(tp,yp,t,y);
    plot(handles.errorPlot,t,ploterror);
    xlabel(handles.errorPlot,'t(days)');
    ylabel(handles.errorPlot,'Error');
    title(handles.errorPlot,'Error Graph');
    plot(handles.perturbPlot,perturbArray,x1Array)
    xlabel(handles.perturbPlot,'Perturbance');
    ylabel(handles.perturbPlot,'Error');
    title(handles.perturbPlot,'Perturbance vs. Error');
end
end
